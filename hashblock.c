/*
 #Copyright (c) 2011, Joseph Matheney
 #All rights reserved.
 
 #Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 
 #    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 #    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the 
documentation and/or other materials provided with the distribution.
 
 #THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE.
 */

#ifdef fail
#!/bin/bash
# NOTE you can chmod 0755 this file and then execute it to compile (or just copy and paste)
gcc -o hashblock hashblock.c -lssl
exit 0
#endif

#include <openssl/sha.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// this is the block header, it is 80 bytes long (steal this code)
typedef struct block_header {
    unsigned int    version;
    // dont let the "char" fool you, this is binary data not the human readable version
    unsigned char   prev_block[32];
    unsigned char   merkle_root[32];
    unsigned int    timestamp;
    unsigned int    bits;
    unsigned int    nonce;
} block_header;


// we need a helper function to convert hex to binary, this function is unsafe and slow, but very readable (write something better)
void hex2bin(unsigned char* dest, unsigned char* src)
{
    unsigned char bin;
    int c, pos;
    char buf[3];
    
    pos=0;
    c=0;
    buf[2] = 0;
    while(c < strlen(src))
    {
        // read in 2 characaters at a time
        buf[0] = src[c++];
        buf[1] = src[c++];
        // convert them to a interger and recast to a char (uint8)
        dest[pos++] = (unsigned char)strtol(buf, NULL, 16);
    }
    
}

// this function is mostly useless in a real implementation, were only using it for demonstration purposes
void hexdump(unsigned char* data, int len)
{
    int c;
    
    c=0;
    while(c < len)
    {
        printf("%.2x", data[c++]);
    }
    printf("\n");
}

// this function swaps the byte ordering of binary data, this code is slow and bloated (write your own)
void byte_swap(unsigned char* data, int len) {
    int c;
    unsigned char tmp[len];
    
    c=0;
    while(c<len)
    {
        tmp[c] = data[len-(c+1)];
        c++;
    }
    
    c=0;
    while(c<len)
    {
        data[c] = tmp[c];
        c++;
    }
}

