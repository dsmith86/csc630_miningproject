#include "hashblock.c"
#include <limits.h>
#include <mpi.h>
#include <time.h>

#define VERSION 1
#define PREV_BLOCK "0000000000000000000000000000000000000000000000000000000000000000"
#define MERKLE_ROOT "4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b"
#define TARGET_HASH "00000000ffff0000000000000000000000000000000000000000000000000000"
#define TIMESTAMP 1231006505
#define BITS 486604799

unsigned int const SEARCH_MIN = 2078236893;//0;
unsigned int const SEARCH_MAX = 2088236893;//ULONG_MAX;

unsigned int mine(block_header header, unsigned char* target, unsigned int min, unsigned int max);
int hex_smaller(unsigned char* hash1, unsigned char* hash2, int len);

int main() {
	int my_rank, comm_sz;
	double time;

    // start with a block header struct
    block_header header;
    
    // we are going to supply the block header with the values from the generation block 0
    header.version = VERSION;
    hex2bin(header.prev_block, PREV_BLOCK);
	hex2bin(header.merkle_root, MERKLE_ROOT);
    header.timestamp = TIMESTAMP;
    header.bits = BITS;

	unsigned char target_hash[32];
	hex2bin(target_hash, "00000000ffff0000000000000000000000000000000000000000000000000000");

	MPI_Init(NULL, NULL);

	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);

	unsigned int range = SEARCH_MAX - SEARCH_MIN;
	
	unsigned int local_range = range / comm_sz;
	if (my_rank == comm_sz - 1)
	{
		//local_range += range % comm_sz;
	}

	unsigned int success = -1;
	unsigned int local_min = SEARCH_MIN + my_rank * local_range;
	unsigned int local_max = local_min + local_range;

	printf("processor %lu: min = %lu, max = %lu\n", my_rank, local_min, local_max);

	time = MPI_Wtime();
	success = mine(header, target_hash, local_min, local_max);
	time = MPI_Wtime() - time;

	if (success >= 0)
	{
		printf("Process %d found the nonce %d in %.2f seconds.\n", my_rank, success, time);
		MPI_Abort(MPI_COMM_WORLD, 0);    				
	}
	else
	{
		printf("Process %d did not find the nonce and took %.2f seconds to search.\n", my_rank, time);
	}

	MPI_Finalize();
	
    return 0;
}

unsigned int mine(block_header header, unsigned char* target, unsigned int min, unsigned int max)
{
	// we need a place to store the checksums
    unsigned char hash1[SHA256_DIGEST_LENGTH];
    unsigned char hash2[SHA256_DIGEST_LENGTH];

	// you should be able to reuse these, but openssl sha256 is slow, so your probbally not going to implement this anyway
    SHA256_CTX sha256_pass1, sha256_pass2;

	// the endianess of the checksums needs to be little, this swaps them form the big endian format you normally see in block explorer
    byte_swap(header.prev_block, 32);
    byte_swap(header.merkle_root, 32);

	unsigned int i;
	for (i = min; i <= max; i++)
	{
		header.nonce = i;

		// Use SSL's sha256 functions, it needs to be initialized
    	SHA256_Init(&sha256_pass1);
	    // then you 'can' feed data to it in chuncks, but here were just making one pass cause the data is so small
	    SHA256_Update(&sha256_pass1, (unsigned char*)&header, sizeof(block_header));
	    // this ends the sha256 session and writes the checksum to hash1
	    SHA256_Final(hash1, &sha256_pass1);

		//same as above
	    SHA256_Init(&sha256_pass2);
	    SHA256_Update(&sha256_pass2, hash1, SHA256_DIGEST_LENGTH);
	    SHA256_Final(hash2, &sha256_pass2);

		byte_swap(hash2, SHA256_DIGEST_LENGTH);

		if (hex_smaller(hash2, target, SHA256_DIGEST_LENGTH))
		{
			return i;
		}	
	}

	return -1;
}

int hex_smaller(unsigned char* hash1, unsigned char* hash2, int len)
{
	int i;

	for (i = 0; i < len; i++)
	{
		if (hash1[i] != hash2[i])
		{
			if (hash1[i] > hash2[i])
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
	}
	return 1;
}
