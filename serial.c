#include "hashblock.c"
#include <limits.h>
#include <time.h>

#define SEARCH_MIN 2078236893
#define SEARCH_MAX 2088236893
#define RUN_INTERVAL 1000

int mine(block_header header, unsigned char* target, int min, int max);
int hex_smaller(unsigned char* hash1, unsigned char* hash2, int len);

int main() {
	clock_t begin, end;
	double time_spent;

    // start with a block header struct
    block_header header;
    
    // we are going to supply the block header with the values from the generation block 0
    header.version =        1;
    hex2bin(header.prev_block,              "0000000000000000000000000000000000000000000000000000000000000000");
    hex2bin(header.merkle_root,             "4a5e1e4baab89f3a32518a88c31bc87f618f76673e2cc77ab2127b7afdeda33b");
    header.timestamp =      1231006505;
    header.bits =           486604799;

	unsigned char* target_hash;
	hex2bin(target_hash, "00000000ffff0000000000000000000000000000000000000000000000000000");

	begin = clock();
	int success = mine(header, target_hash, SEARCH_MIN, SEARCH_MAX);
	end = clock();

	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

	printf("Found the nonce %d in %.2f seconds.\n", success, time_spent);    
	printf("Range: %d.\n", SEARCH_MAX - SEARCH_MIN);

	float range = SEARCH_MAX - SEARCH_MIN;
	printf("Average time per %d: %f seconds.\n", RUN_INTERVAL, time_spent / ((success - SEARCH_MIN) / range) / range * RUN_INTERVAL);

    return 0;
}

int mine(block_header header, unsigned char* target, int min, int max)
{
	// we need a place to store the checksums
    unsigned char hash1[SHA256_DIGEST_LENGTH];
    unsigned char hash2[SHA256_DIGEST_LENGTH];

	// you should be able to reuse these, but openssl sha256 is slow, so your probbally not going to implement this anyway
    SHA256_CTX sha256_pass1, sha256_pass2;

	// the endianess of the checksums needs to be little, this swaps them form the big endian format you normally see in block explorer
    byte_swap(header.prev_block, 32);
    byte_swap(header.merkle_root, 32);

	int i;
	for (i = min; i <= max; i++)
	{
		header.nonce = i;

		// Use SSL's sha256 functions, it needs to be initialized
    	SHA256_Init(&sha256_pass1);
	    // then you 'can' feed data to it in chuncks, but here were just making one pass cause the data is so small
	    SHA256_Update(&sha256_pass1, (unsigned char*)&header, sizeof(block_header));
	    // this ends the sha256 session and writes the checksum to hash1
	    SHA256_Final(hash1, &sha256_pass1);

		//same as above
	    SHA256_Init(&sha256_pass2);
	    SHA256_Update(&sha256_pass2, hash1, SHA256_DIGEST_LENGTH);
	    SHA256_Final(hash2, &sha256_pass2);

		byte_swap(hash2, SHA256_DIGEST_LENGTH);

		if (hex_smaller(hash2, target, SHA256_DIGEST_LENGTH))
		{
			return i;
		}	

		int range = max - min;
		int progress = i - min;
		
		if (i % 10000000 == 0)
		{
			printf("%.2f%% done\n", (float)progress/range*100);
		}
	}

	return -1;
}

int hex_smaller(unsigned char* hash1, unsigned char* hash2, int len)
{
	int i;

	for (i = 0; i < len; i++)
	{
		if (hash1[i] != hash2[i])
		{
			if (hash1[i] > hash2[i])
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
	}
	return 1;
}
